#include <sourcemod>
#include <sdktools>

#define PLUGIN_VERSION  "1.0"

public Plugin myinfo =
{
	name = "No Frag Loss",
	author = "Zipcore",
	description = "",
	version = PLUGIN_VERSION,
	url = "zipcore#googlemail.com"
};

public OnPluginStart()
{
	HookEventEx("player_death", Event_Death);
}

public Event_Death(Handle event, const char[] name, bool broadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

	if (client == attacker || attacker == 0)
	{
		SetEntProp(client, Prop_Data, "m_iFrags", GetClientFrags(client)+1);
	}
}